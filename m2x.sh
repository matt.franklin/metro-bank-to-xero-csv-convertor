#!/bin/bash

# METRO BANK TO XERO CSV CONVERTOR

# The purpose of this script is to process Metro Bank CSV statement so that they are ready to manually import into Xero.
# Run this script with parameter #1 being the input file (directly downloaded from Metro Bank) and parameter #2 being the output file location
# The output file will then be ready for import into Xero. 

# Check if input and output file paths are provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 input_file output_file"
    exit 1
fi

# Input and output file paths 
input_file="$1"
output_file="$2"

# Make the outgoing column (E) negative by multiplying by -1
awk -F, 'BEGIN {OFS=","} NR==1 {print} NR>1 {$5=-$5; print}' "$input_file" > temp.csv

# Replace Column F with the sum of column D and column E, and change the header to 'Transaction Amount'
awk -F, 'BEGIN {OFS=","} 
NR==1 {
    $6="Transaction Amount"
    print
} 
NR>1 {
    $6=($4=="" ? 0 : $4) + ($5=="" ? 0 : $5)
    print
}' temp.csv > temp_combined.csv

# Remove columns D and E and rename 'Column B' to 'Description'
awk -F, 'BEGIN {OFS=","} 
NR==1 {
    $2="Description"
    print $1, $2, $3, $6
} 
NR>1 {
    print $1, $2, $3, $6
}' temp_combined.csv > "$output_file"

# Clean up temporary files
rm temp.csv temp_combined.csv

echo "Preparation complete. The modified file is saved as $output_file."
echo "The modified file is ready for import to Xero."