# Metro Bank to Xero CSV convertor

## Description

This bash script is designed to convert the CSV bank statement file provided by Metro Bank to a format which can be easily imported by Xero. The Metro Bank CSV statement includes the following columns:

* Date (Column A)
* Details (Column B)
* Transaction Type (Column C)
* In (Column D)
* Out (Column E)
* Balance (Column F)

Of note, is that Column E (Out) includes the outgoing transaction amount as a postive figure. 

Importing to Xero accepts the following data fields:

* Date
* Amount (total transaction amount - negative for outgoing, positive for incoming)
* Payee
* Description
* Reference
* Check Number

Not all of these are available in the Metro Bank export, and so can be ignored.

This script uses the available data from Metro Bank and assigns it to relevant fields from the Xero schema. 

### Processing Steps
1. Make the outgoing column (E) negative: The script multiplies the contents of column E by -1.
2. Combine columns D and E into a new column:
  * Column F is replaced with the sum of columns D and E.
  * The header of this new column is renamed to 'Transaction Amount'.
3. Remove columns D and E.
4. Rename columns:
 * Rename 'Column B' to 'Description'.
5. Delete the empty Column D.


## Getting Started

### Dependencies

* Bash shell
* `awk` utility 

### Installing

1. Download the script to a local directory
```bash
git clone https://gitlab.com/matt.franklin/metro-bank-to-xero-csv-convertor.git
cd metro-bank-to-xero-csv-convertor/
```
2. Ensure that the script has execution permissions. You can do this with the command: 
```bash
chmod +x m2x.sh
```

### Executing program

* Run this script with the first parameter being the input file (directly downloaded from Metro Bank) and the second parameter being the output file location. The output file will then be ready for import into Xero.
```bash
./m2x.sh input_file.csv output_file.csv
```
## Cleanup
Temporary files created during the processing are removed automatically. If the script is interrupted it may be necessary to clear the temporary files created in teh directory where the script is run. This can be done with the command:
```bash
rm temp.csv temp_combined.csv
```

## Authors

Matt Franklin

## Version History

* 0.1
    * Initial Release

## License

This project is licensed under the MIT License - see the LICENSE.md file for details